from decimal import Decimal, InvalidOperation

def collatz(x):
    current_value = x
    while current_value > 1:
        yield current_value
        if current_value % 2 == 0:
            current_value = current_value / 2
        else:
            current_value = 3 * current_value + 1


def stopping_time(x):
    total_stopping_time = 0
    for val in collatz(x):
        total_stopping_time += 1
    return total_stopping_time


if __name__ == '__main__':
    import sys

    if len(sys.argv) < 3:
        print('Usage: python {} <mode> <starting number>'.format(__file__))
        sys.exit(1)

    mode = sys.argv[-2]
    if mode not in ('seq', 'inf'):
        print('Mode must be "seq" or "inf"')
        sys.exit(1)

    try:
        starting_number = Decimal(sys.argv[-1])
    except InvalidOperation:
        print('Staring number must be an integer')
        sys.exit(1)

    if mode == 'seq':
        st = 0
        for val in collatz(starting_number):
            print(val)
            st += 1
        print('Stopping time: {}'.format(st))

    elif mode == 'inf':
        k = starting_number
        times = set()
        while True:
            sys.stdout.write('\r{}'.format(k))
            st = stopping_time(k)
            if st not in times:
                times.add(st)
                print('\rNew stopping time at {}: {} ({} total)'.format(k, st, len(times)))
            k += 1
